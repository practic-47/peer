FROM debian:stable-slim
EXPOSE 8080
COPY target/release/peer ./app
CMD ["./app"]
