use async_std::task;
use async_trait::async_trait;
use http::status::StatusCode;
use std::{convert::Infallible, thread};

pub struct World {
    response: Option<StatusCode>,
}

impl Default for World {
    fn default() -> Self {
        World {
            response: Option::None,
        }
    }
}

#[async_trait(?Send)]
impl cucumber::World for World {
    type Error = Infallible;

    async fn new() -> Result<Self, Infallible> {
        Ok(Self::default())
    }
}

mod courses {
    use crate::World;
    use cucumber::Steps;
    use http::status::StatusCode;

    pub fn steps() -> Steps<World> {
        let mut builder: Steps<World> = Steps::new();
        builder
            .when("User creates a new Course", |mut world, _step| {
                let response = isahc::put("http://127.0.0.1:8080/courses", "")
                    .expect("Failed to send request.");
                world.response = Some(response.status());
                world
            })
            .then("Success result received", |world, _step| {
                assert!(StatusCode::OK.is_success());
                world
            });
        builder
    }
}

mod orders {
    use crate::World;
    use cucumber::Steps;
    use http::status::StatusCode;

    pub fn steps() -> Steps<World> {
        let mut builder: Steps<World> = Steps::new();
        builder
            .when("User submits an Order", |mut world, _step| {
                let response = isahc::put("http://127.0.0.1:8080/orders", "")
                    .expect("Failed to send request.");
                world.response = Some(response.status());
                world
            })
            .then("Success result received", |world, _step| {
                assert!(StatusCode::OK.is_success());
                world
            });
        builder
    }
}

mod users {
    use crate::World;
    use cucumber::Steps;
    use http::status::StatusCode;

    pub fn steps() -> Steps<World> {
        let mut builder: Steps<World> = Steps::new();
        builder
            .when("User registers a new User", |mut world, _step| {
                let response =
                    isahc::put("http://127.0.0.1:8080/users", "").expect("Failed to send request.");
                world.response = Some(response.status());
                world
            })
            .then("Success result received", |world, _step| {
                assert!(StatusCode::OK.is_success());
                world
            });
        builder
    }
}

fn main() {
    thread::spawn(|| {
        use domain::{courses::Course, orders::Order, users::User};
        use dotenv::dotenv;
        use peer::State;
        use std::env;
        use tide::{prelude::*, Request};
        task::block_on(async {
            dotenv().ok();
            tide::log::start();
            let mut app = tide::with_state(State::new());
            app.at("/").get(|_| async {
                Ok(json!({
                    "_links": {
                        "app:courses": { "href": "/courses/" },
                        "app:orders": { "href": "/orders/" },
                        "app:users": { "href": "/users/" },
                        "curie": {
                            "href": "127.0.0.1:8080/{rel}",
                            "name": "app",
                            "templated": "true"
                        },
                        "self": { "href": "/" }
                    }
                }))
            });
            app.at("/courses").get(|request: Request<State>| async move { Ok(request.state().courses().read_all())})
        .put(|request: Request<State>| async move { request.state().courses().create(Course::new("Rust".to_string())); Ok("200") });
            app.at("/courses/:id")
                .get(|request: Request<State>| async move {
                    Ok(request.state().courses().read(request.param("id").unwrap()))
                })
                .delete(|request: Request<State>| async move {
                    request
                        .state()
                        .courses()
                        .delete(request.param("id").unwrap());
                    Ok("200")
                });
            app.at("/orders").get(|request: Request<State>| async move { Ok(request.state().orders().read_all()) })
        .put(|request: Request<State>| async move { request.state().orders().create(Order::new()); Ok("200") });
            app.at("/orders/:id")
                .get(|request: Request<State>| async move {
                    Ok(request.state().orders().read(request.param("id").unwrap()))
                })
                .delete(|request: Request<State>| async move {
                    request
                        .state()
                        .orders()
                        .delete(request.param("id").unwrap());
                    Ok("200")
                });
            app.at("/users")
                .get(
                    |request: Request<State>| async move { Ok(request.state().users().read_all()) },
                )
                .put(|request: Request<State>| async move {
                    request.state().users().create(User::new("Frank"));
                    Ok("200")
                });
            app.at("/users/:id")
                .get(|request: Request<State>| async move {
                    Ok(request.state().users().read(request.param("id").unwrap()))
                })
                .delete(|request: Request<State>| async move {
                    request.state().users().delete(request.param("id").unwrap());
                    Ok("200")
                });
            app.listen(env::var("SERVICE_URL").unwrap_or_else(|_| "127.0.0.1:8080".to_string()))
                .await
                .expect("Failed to start the app.");
        });
    });
    let runner = cucumber::Cucumber::<World>::new()
        .features(&["./features"])
        .steps(courses::steps())
        .steps(orders::steps())
        .steps(users::steps());
    task::block_on(runner.run());
}
