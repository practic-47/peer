use domain::{courses::Course, orders::Order, users::User};
use maturest::Resource;
use std::sync::{Arc, Mutex, MutexGuard};

#[derive(Clone)]
pub struct State {
    users: Arc<Mutex<Resource<User>>>,
    courses: Arc<Mutex<Resource<Course>>>,
    orders: Arc<Mutex<Resource<Order>>>,
}

impl State {
    pub fn new() -> Self {
        State {
            users: Arc::new(Mutex::new(Resource::<User>::new())),
            courses: Arc::new(Mutex::new(Resource::<Course>::new())),
            orders: Arc::new(Mutex::new(Resource::<Order>::new())),
        }
    }

    pub fn users(&self) -> MutexGuard<'_, Resource<User>> {
        self.users.lock().unwrap()
    }

    pub fn courses(&self) -> MutexGuard<'_, Resource<Course>> {
        self.courses.lock().unwrap()
    }

    pub fn orders(&self) -> MutexGuard<'_, Resource<Order>> {
        self.orders.lock().unwrap()
    }
}

impl Default for State {
    fn default() -> Self {
        Self::new()
    }
}
