use domain::{courses::Course, orders::Order, users::User};
use dotenv::dotenv;
use peer::State;
use std::env;
use tide::{prelude::*, Request};

#[async_std::main]
async fn main() -> Result<(), std::io::Error> {
    dotenv().ok();
    tide::log::start();
    let mut app = tide::with_state(State::new());
    app.at("/").get(|_| async {
        Ok(json!({
            "_links": {
                "app:courses": { "href": "/courses/" },
                "app:orders": { "href": "/orders/" },
                "app:users": { "href": "/users/" },
                "curie": {
                    "href": "127.0.0.1:8080/{rel}",
                    "name": "app",
                    "templated": "true"
                },
                "self": { "href": "/" }
            }
        }))
    });
    app.at("/courses")
        .get(|request: Request<State>| async move { Ok(request.state().courses().read_all()) })
        .put(|request: Request<State>| async move {
            request
                .state()
                .courses()
                .create(Course::new("Rust".to_string()));
            Ok("200")
        });
    app.at("/courses/:id")
        .get(|request: Request<State>| async move {
            Ok(request.state().courses().read(request.param("id").unwrap()))
        })
        .delete(|request: Request<State>| async move {
            request
                .state()
                .courses()
                .delete(request.param("id").unwrap());
            Ok("200")
        });
    app.at("/orders")
        .get(|request: Request<State>| async move { Ok(request.state().orders().read_all()) })
        .put(|request: Request<State>| async move {
            request.state().orders().create(Order::new());
            Ok("200")
        });
    app.at("/orders/:id")
        .get(|request: Request<State>| async move {
            Ok(request.state().orders().read(request.param("id").unwrap()))
        })
        .delete(|request: Request<State>| async move {
            request
                .state()
                .orders()
                .delete(request.param("id").unwrap());
            Ok("200")
        });
    app.at("/users")
        .get(|request: Request<State>| async move { Ok(request.state().users().read_all()) })
        .put(|request: Request<State>| async move {
            request.state().users().create(User::new("Frank"));
            Ok("200")
        });
    app.at("/users/:id")
        .get(|request: Request<State>| async move {
            Ok(request.state().users().read(request.param("id").unwrap()))
        })
        .delete(|request: Request<State>| async move {
            request.state().users().delete(request.param("id").unwrap());
            Ok("200")
        });
    app.listen(env::var("SERVICE_URL").unwrap_or_else(|_| "127.0.0.1:8080".to_string()))
        .await?;
    Ok(())
}
